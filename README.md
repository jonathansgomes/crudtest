## CRUD TEST rest api with H2 DB

Consume and resize photos, listing the resized photos on a rest api

### Installation and Running
* mvn spring-boot:run

### Test
* mvn test

## Basic usage

* application runs on port 8080

### Create a product:

* POST request to http://localhost:8080/product/new 
Example Request:
Content-Type →application/json
{
	"name":"Product 1",
	"description":"Product 1"
}
Example Response:
true

### Update a product:

* POST request to http://localhost:8080/product/update
Example Request:
Content-Type →application/json
{
	"id":1
	"name":"new Product 1",
	"description":"new Product 1"
}
Example Response:
true

### Delete a product:

* POST request to http://localhost:8080/product/delete
Example Request:
Content-Type →application/json
{
	"id":1
}
Example Response:
true

### List products:

* GET request to http://localhost:8080/product/list
Optional URL parameters:
id: int id of a product to be listed
subProducts: boolean flag to list subproducts
images: boolean flag to list images

Example Response:
[
	{
		"id": 1,
		"name": "Product 1",
		"description": "Product 1",
		"parentProduct": null,
		"childProducts": null,
		"images": null
	}
]

### List sub-products:

* POST request to http://localhost:8080/product/listSubProducts
Example Request:
Content-Type →application/json
{
	"id":1
}
Example Response:
[
  {
    "id": 2,
    "name": "Product 2",
    "description": "Product 2",
    "parentProduct": {
      "id": 1,
      "name": "Product 1",
      "description": "Product 1",
      "parentProduct": null,
      "childProducts": null,
      "images": null
    },
    "childProducts": null,
    "images": null
  }
]

### Create a image:

* POST request to http://localhost:8080/image/new 
Example Request:
Content-Type →application/json
{
	"type":"jpg",
	"product":{
		"id":1
	}
}
Example Response:
true

### Update a image:

* POST request to http://localhost:8080/image/update
Example Request:
Content-Type →application/json
{
	"id":1
	"type":"png"
}
Example Response:
true

### Delete a image:

* POST request to http://localhost:8080/image/delete
Example Request:
Content-Type →application/json
{
	"id":1
}
Example Response:
true

### List images:

* GET request to http://localhost:8080/image/list

Example Response:
[
	{
		"id": 1,
		"type": "jpg",
		"product": {
			"id": 1,
			"name": "Product 1",
			"description": "Product 1",
			"parentProduct": null,
			"childProducts": null,
			"images": null
		}
	}
]

### List images by product:

* POST request to http://localhost:8080/image/listImagesByProduct
Example Request:
Content-Type →application/json
{
	"id":1
}
Example Response:
[
  {
    "id": 1,
    "type": "jpg",
    "product": {
      "id": 1,
      "name": "Product 1",
      "description": "Product 1",
      "parentProduct": null,
      "childProducts": null,
      "images": null
    }
  }
]