package br.com.jonathangomes.image;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.jonathangomes.image.controller.ImageController;
import br.com.jonathangomes.image.vo.ImageVO;
import br.com.jonathangomes.product.controller.ProductController;
import br.com.jonathangomes.product.vo.ProductVO;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ImageTest {
	@Autowired
	ImageController imageController;
	@Autowired
	ProductController productController;
	

    @Test
    public void contexLoads() throws Exception {
    	assertThat(imageController).isNotNull();
    	assertThat(productController).isNotNull();
    }
	
	@Test
	public void insertImageShouldReturnTrue() {
		assertThat(productController.insertProduct(getNewProduct())).isEqualTo(true);
		ProductVO p = productController.listProducts(false, false, 0).get(0);
		assertThat(imageController.insertImage(getNewImage(p))).isEqualTo(true);
	}
	@Test
	public void listProductShouldReturnArrayOfImages() {
		assertThat(imageController.listImages().getClass()).isEqualTo(ArrayList.class);
	}
	@Test
	public void updateProductShouldReturnTrue() {
		assertThat(productController.insertProduct(getNewProduct())).isEqualTo(true);
		ProductVO p = productController.listProducts(false, false, 0).get(0);
		
		assertThat(imageController.insertImage(getNewImage(p))).isEqualTo(true);
		ImageVO i = imageController.listImages().get(0);
		i.setType("New Type");
		assertThat(imageController.updateImage(i)).isEqualTo(true);
	}
	
	private ImageVO getNewImage(ProductVO p) {
		ImageVO i = new ImageVO();
		i.setType("JPG");
		i.setProduct(p);
		return i;
	}
	private ProductVO getNewProduct() {
		ProductVO p = new ProductVO();
		p.setName("Product 1");
		p.setDescription("Product 1");
		return p;
	}
}
