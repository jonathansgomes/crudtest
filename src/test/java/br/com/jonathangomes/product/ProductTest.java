package br.com.jonathangomes.product;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.jonathangomes.product.controller.ProductController;
import br.com.jonathangomes.product.vo.ProductVO;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductTest {
	@Autowired
	ProductController productController;
	

    @Test
    public void contexLoads() throws Exception {
    	assertThat(productController).isNotNull();
    }
	
	@Test
	public void insertProductShouldReturnTrue() {
		assertThat(productController.insertProduct(getNewProduct())).isEqualTo(true);
	}
	@Test
	public void listProductShouldReturnArrayOfProducts() {
		assertThat(productController.listProducts(false, false, 0).getClass()).isEqualTo(ArrayList.class);
	}
	@Test
	public void updateProductShouldReturnTrue() {
		assertThat(productController.insertProduct(getNewProduct())).isEqualTo(true);
		ProductVO p = productController.listProducts(false, false, 0).get(0);
		p.setName("New Name");
		assertThat(productController.updateProduct(p)).isEqualTo(true);
	}

	private ProductVO getNewProduct() {
		ProductVO p = new ProductVO();
		p.setName("Product 1");
		p.setDescription("Product 1");
		return p;
	}
}
