package br.com.jonathangomes.product.vo;

import java.io.Serializable;
import java.util.List;

import br.com.jonathangomes.image.vo.ImageVO;

public class ProductVO implements Serializable{
	private static final long serialVersionUID = 1L;

	private int id;
	private String name;
	private String description;
    private ProductVO parentProduct;
    private List<ProductVO> childProducts;
    private List<ImageVO> images;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	

	public ProductVO getParentProduct() {
		return parentProduct;
	}

	public void setParentProduct(ProductVO parentProduct) {
		this.parentProduct = parentProduct;
	}


	public List<ProductVO> getChildProducts() {
		return childProducts;
	}

	public void setChildProducts(List<ProductVO> childProducts) {
		this.childProducts = childProducts;
	}

	public List<ImageVO> getImages() {
		return images;
	}

	public void setImages(List<ImageVO> images) {
		this.images = images;
	}

}
