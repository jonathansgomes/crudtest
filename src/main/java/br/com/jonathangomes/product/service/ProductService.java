package br.com.jonathangomes.product.service;

import java.util.List;

import br.com.jonathangomes.product.vo.ProductVO;

public interface ProductService {

	List<ProductVO> listProducts(boolean subProducts, boolean images, int id);

	boolean insertProduct(ProductVO product);
	
	List<ProductVO> listSubProducts(ProductVO product);

	boolean deleteProduct(ProductVO product);

	boolean updateProduct(ProductVO product);

}
