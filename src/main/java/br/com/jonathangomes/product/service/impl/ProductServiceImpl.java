package br.com.jonathangomes.product.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.jonathangomes.image.converter.ImageConverter;
import br.com.jonathangomes.product.converter.ProductConverter;
import br.com.jonathangomes.product.dao.ProductDao;
import br.com.jonathangomes.product.model.Product;
import br.com.jonathangomes.product.service.ProductService;
import br.com.jonathangomes.product.vo.ProductVO;

@Service
public class ProductServiceImpl implements ProductService {
	
	@Autowired
	private ProductDao productDao;

	public List<ProductVO> listProducts(boolean subProducts, boolean images, int id){
		List<Product> list = productDao.listProducts(subProducts, images, id);
		List<ProductVO> voList = ProductConverter.getVOList(list);
		if((subProducts || images) && voList != null){
			for(int i = 0; i <  voList.size(); i++){
				if(subProducts && images){
					voList.get(i).setChildProducts(listSubProducts(voList.get(i)));
					voList.get(i).setImages(ImageConverter.getVOList(list.get(i).getImages()));
				}else if(subProducts && !images){
					voList.get(i).setChildProducts(ProductConverter.getVOList(list.get(i).getChildProducts()));
				}else if(!subProducts && images){
					voList.get(i).setImages(ImageConverter.getVOList(list.get(i).getImages()));
				}
			}
		}
		return voList;
	}

	public List<ProductVO> listSubProducts(ProductVO product) {        
		return ProductConverter.getVOList(productDao.listSubProducts(ProductConverter.getModel(product)));
	}


	public boolean insertProduct(ProductVO product) {        
		return productDao.insertProduct(ProductConverter.getModel(product));
	}

	@Override
	public boolean deleteProduct(ProductVO product) {
		return productDao.deleteProduct(ProductConverter.getModel(product));
	}

	@Override
	public boolean updateProduct(ProductVO product) {
		return productDao.updateProduct(ProductConverter.getModel(product));
	}

}
