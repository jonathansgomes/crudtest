package br.com.jonathangomes.product.converter;

import java.util.ArrayList;
import java.util.List;

import br.com.jonathangomes.product.model.Product;
import br.com.jonathangomes.product.vo.ProductVO;

public class ProductConverter {
	
	public static List<ProductVO> getVOList(List<Product> productModels){
		List<ProductVO> productVOs = new  ArrayList<ProductVO>();
		if(productModels != null && !productModels.isEmpty()){
			for(Product p : productModels){
				ProductVO vo = getVO(p);
				if(vo != null){				
					productVOs.add(vo);
				}
			}
		}
		return productVOs;
	}
	public static ProductVO getVO(Product productModel){
		ProductVO vo = null;
		if(productModel != null){
			vo = new ProductVO();
			vo.setId(productModel.getId());
			vo.setName(productModel.getName());
			vo.setDescription(productModel.getDescription());
			vo.setParentProduct(getVO(productModel.getParentProduct()));
		}
		return vo;
	}

	public static Product getModel(ProductVO vo){
		Product productModel = null;
		if(vo != null){
			productModel = new Product();
			productModel.setId(vo.getId());
			productModel.setName(vo.getName());
			productModel.setDescription(vo.getDescription());
			productModel.setParentProduct(getModel(vo.getParentProduct()));
		}
		return productModel;
	}
}
