package br.com.jonathangomes.product.dao;

import java.util.List;

import br.com.jonathangomes.product.model.Product;

public interface ProductDao {
	
	List<Product> listProducts(boolean subProducts, boolean images, int id);
	List<Product> listSubProducts(Product product);
	boolean insertProduct(Product product);
	Product product(Product product);
	boolean deleteProduct(Product model);
	boolean updateProduct(Product model);

}
