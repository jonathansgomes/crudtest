package  br.com.jonathangomes.product.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import br.com.jonathangomes.image.model.Image;
import br.com.jonathangomes.product.dao.ProductDao;
import br.com.jonathangomes.product.model.Product;

@Repository
public class ProductDaoImpl implements ProductDao {

	@PersistenceContext
    private EntityManager entityManager;
	
	public List<Product> listProducts(boolean subProducts, boolean images, int id){
		try{
			StringBuilder sql = new StringBuilder("FROM Product p");
	
			if(images){
				sql.append(" LEFT JOIN FETCH p.images ");
			}else if(subProducts){
				sql.append(" LEFT JOIN FETCH p.childProducts ");
			}
			if(id  > 0){
				sql.append(" WHERE p.id = :id ");
			}
			Query query = entityManager.createQuery(sql.toString());
	
			if(id  > 0){
				query.setParameter("id", id);
			}
			return query.getResultList();
		}catch(Exception e){
			System.out.println("ERROR LISTING PRODUCT: "+ e);	
		}
		return new ArrayList<Product>();
	}

	public List<Product> listSubProducts(Product product) {   
		try{
			TypedQuery<Product> query = entityManager.createQuery("SELECT p FROM Product p WHERE p.parentProduct.id = :id", Product.class);  
			query.setParameter("id", product.getId());
			return query.getResultList();
		}catch(Exception e){
			System.out.println("ERROR LISTING PRODUCT: "+ e);	
		}
		return new ArrayList<Product>();
	}

	@Override
	public Product product(Product product) {   
		try{
	    	TypedQuery<Product> query = entityManager.createQuery("SELECT p FROM Product p WHERE p.id = :id", Product.class);  
			query.setParameter("id", product.getId());
			return query.getSingleResult();
		}catch(Exception e){
			System.out.println("ERROR LISTING PRODUCT: "+ e);	
		}
		return null;
	}
	
	public boolean insertProduct(Product product) {
		try{
			if(hasParent(product)){			
				Product p = entityManager.find(Product.class,product.getParentProduct().getId());
			    if(p==null){
			        return false;
			    }
			}
		    entityManager.persist(product);
		    return true;
		}catch(Exception e){
			System.out.println("ERROR INSERTING PRODUCT: "+ e);	
		}
	    return true;
	}
	private boolean hasParent(Product product){
		return product != null && product.getParentProduct() != null && product.getParentProduct().getId() > 0;
	}

	@Override
	public boolean deleteProduct(Product product) {
		try{
			if(hasChildrenObjects(product)){
				return false;
			}
			if(product != null && product.getId() > 0){
				Query query = entityManager.createQuery(
					      "DELETE FROM Product p WHERE p.id = :id");
			  int deletedCount = query.setParameter("id", product.getId()).executeUpdate();
			  return deletedCount > 0;
			}		
		}catch(Exception e){
			System.out.println("ERROR DELETING PRODUCT: "+ e);			
		}			
		return false;
	}

	private boolean hasChildrenObjects(Product product) {
		List<Product> subProducts = listSubProducts(product);
		List<Image> images = listImagesByProduct(product);
		return (subProducts != null && !subProducts.isEmpty())
				|| (images != null && !images.isEmpty());
	}
	
	private List<Image> listImagesByProduct(Product product) {
		try{
	    	TypedQuery<Image> query = entityManager.createQuery("SELECT i FROM Image i WHERE i.product.id = :id", Image.class);  
			query.setParameter("id", product.getId());
			return query.getResultList();
		}catch(Exception e){
			System.out.println("ERROR LISTING PRODUCT IMAGES: "+ e);	
		}
		return new ArrayList<Image>();
	}
	@Override
	public boolean updateProduct(Product product) {
		try{
			if(product != null && product.getId() > 0){
				StringBuilder sql = new StringBuilder("UPDATE Product p SET ");
				if(product.getName() != null && !product.getName().isEmpty()){
					sql.append(" p.name = :name ");
				}
				if(product.getDescription() != null && !product.getDescription().isEmpty()){
					sql.append(", p.description = :description ");
				}
				
				sql.append(" WHERE p.id = :id ");
				
				Query query = entityManager.createQuery(sql.toString());
				query.setParameter("id", product.getId());
				
				if(product.getName() != null && !product.getName().isEmpty()){
					query.setParameter("name", product.getName());
				}
				if(product.getDescription() != null && !product.getDescription().isEmpty()){
					query.setParameter("description", product.getDescription());
				}
				int updateCount = query.executeUpdate();
				return updateCount > 0;
			}		
		}catch(Exception e){
			System.out.println("ERROR UDPATING PRODUCT: "+ e);			
		}			
		return false;
	}

}