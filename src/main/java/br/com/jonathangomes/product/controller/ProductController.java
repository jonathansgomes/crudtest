package br.com.jonathangomes.product.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import br.com.jonathangomes.product.service.ProductService;
import br.com.jonathangomes.product.vo.ProductVO;

@Transactional
@Controller
@Path("/product")
public class ProductController {
	
	@Autowired
	private ProductService productService;

	@GET
    @Path("/list")
    @Produces(MediaType.APPLICATION_JSON)
	public List<ProductVO> listProducts(
		@QueryParam("subProducts") boolean subProducts,
		@QueryParam("images") boolean images,
		@QueryParam("id") int id) { 
		return productService.listProducts(subProducts, images, id);
	}

	@POST
    @Path("/listSubProducts")
    @Produces(MediaType.APPLICATION_JSON)
	public List<ProductVO> listSubProducts(ProductVO product) {        
		return productService.listSubProducts(product);
	}

	@POST
    @Path("/new")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
	public boolean insertProduct(ProductVO product) {        
		return productService.insertProduct(product);
	}

	@POST
	@Path("/delete")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public boolean deleteProduct(ProductVO product) {
		return productService.deleteProduct(product);
	}

	@POST
	@Path("/update")
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public boolean updateProduct(ProductVO product) {
		return productService.updateProduct(product);
	}

}