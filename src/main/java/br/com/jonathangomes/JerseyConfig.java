package br.com.jonathangomes;

import org.springframework.stereotype.Component;

import br.com.jonathangomes.image.controller.ImageController;
import br.com.jonathangomes.product.controller.ProductController;

import org.glassfish.jersey.server.ResourceConfig;

@Component
public class JerseyConfig extends ResourceConfig {
    public JerseyConfig() {
        registerServices();
    }
    private void registerServices() {
        register(ProductController.class);
        register(ImageController.class);
    }
}