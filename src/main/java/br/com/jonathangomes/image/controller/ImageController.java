package br.com.jonathangomes.image.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import br.com.jonathangomes.image.service.ImageService;
import br.com.jonathangomes.image.vo.ImageVO;
import br.com.jonathangomes.product.vo.ProductVO;

@Transactional
@Controller
@Path("/image")
public class ImageController {
	
	@Autowired
	private ImageService imageService;

    @GET
    @Path("/list")
    @Produces(MediaType.APPLICATION_JSON)
	public List<ImageVO> listImages() { 
		return imageService.listImages();
	}
	
    @POST
    @Path("/listImagesByProduct")
    @Produces(MediaType.APPLICATION_JSON)
    public List<ImageVO> listImagesByProduct(ProductVO product) {
    	return imageService.listImagesByProduct(product);
	}
    
	@POST
    @Path("/new")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
	public boolean insertImage(ImageVO image) {        
		return imageService.insertImage(image);
	}
	
    @POST
    @Path("/delete")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
	public boolean deleteImage(ImageVO image) {        
		return imageService.deleteImage(image);
	}

    @POST
    @Path("/update")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
	public boolean updateImage(ImageVO image) {        
		return imageService.updateImage(image);
	}

}