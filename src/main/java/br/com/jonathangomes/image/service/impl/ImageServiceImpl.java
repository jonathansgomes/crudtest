package br.com.jonathangomes.image.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.jonathangomes.image.converter.ImageConverter;
import br.com.jonathangomes.image.dao.ImageDao;
import br.com.jonathangomes.image.model.Image;
import br.com.jonathangomes.image.service.ImageService;
import br.com.jonathangomes.image.vo.ImageVO;
import br.com.jonathangomes.product.converter.ProductConverter;
import br.com.jonathangomes.product.vo.ProductVO;

@Service
public class ImageServiceImpl implements ImageService {
	
	@Autowired
	private ImageDao imageDao;

	public List<ImageVO> listImages(){
		List<Image> list = imageDao.listImages();
		return ImageConverter.getVOList(list);
	}


	public boolean insertImage(ImageVO image) {        
		return imageDao.insertImage(ImageConverter.getModel(image));
	}


	@Override
	public boolean deleteImage(ImageVO image) {
		return imageDao.deleteImage(ImageConverter.getModel(image));
	}

	@Override
	public boolean updateImage(ImageVO image) {
		return imageDao.updateImage(ImageConverter.getModel(image));
	}


	@Override
	public List<ImageVO> listImagesByProduct(ProductVO product) {
		return ImageConverter.getVOList(imageDao.listImagesByProduct(ProductConverter.getModel(product)));
	}

}
