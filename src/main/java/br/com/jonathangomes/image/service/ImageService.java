package br.com.jonathangomes.image.service;

import java.util.List;

import br.com.jonathangomes.image.vo.ImageVO;
import br.com.jonathangomes.product.vo.ProductVO;

public interface ImageService {

	List<ImageVO> listImages();

	boolean insertImage(ImageVO image);

	boolean deleteImage(ImageVO image);

	boolean updateImage(ImageVO image);

	List<ImageVO> listImagesByProduct(ProductVO product);

}
