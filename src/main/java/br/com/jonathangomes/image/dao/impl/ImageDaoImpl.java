package  br.com.jonathangomes.image.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import br.com.jonathangomes.image.dao.ImageDao;
import br.com.jonathangomes.image.model.Image;
import br.com.jonathangomes.product.dao.ProductDao;
import br.com.jonathangomes.product.model.Product;

@Repository
public class ImageDaoImpl implements ImageDao {
	
	@Autowired
	private ProductDao productDao;

	@PersistenceContext
    private EntityManager entityManager;
	
	public List<Image> listImages(){
		String query = "FROM Image i";
		return entityManager.createQuery(query).getResultList();
	}

	public boolean insertImage(Image image) {
		boolean hasInserted = false;
		try{
			if(hasParentProduct(image)){			
				Product p = productDao.product(image.getProduct());
			    if(p!=null){
				    entityManager.persist(image);
				    hasInserted = true;
			    }
			}
		}catch(Exception e){
			System.out.println("ERROR INSERTING NEW IMAGE: "+ e);
		}
		return hasInserted;
	}
	private boolean hasParentProduct(Image image){
		return image != null && image.getProduct() != null && image.getProduct().getId() > 0;
	}

	@Override
	public boolean deleteImage(Image image) {
		try{
			if(image != null && image.getId() > 0){
				Query query = entityManager.createQuery(
					      "DELETE FROM Image i WHERE i.id = :id");
			  int deletedCount = query.setParameter("id", image.getId()).executeUpdate();
			  return deletedCount > 0;
			}		
		}catch(Exception e){
			System.out.println("ERROR DELETING IMAGE: "+ e);			
		}			
		return false;
	}

	@Override
	public boolean updateImage(Image image) {
		try{
			if(image != null && image.getId() > 0){
				StringBuilder sql = new StringBuilder("UPDATE Image i SET ");
				if(image.getType() != null && !image.getType().isEmpty()){
					sql.append(" i.type = :type ");
				}
				sql.append(" WHERE i.id = :id ");
				
				Query query = entityManager.createQuery(sql.toString());
				query.setParameter("id", image.getId());
				if(image.getType() != null && !image.getType().isEmpty()){
					query.setParameter("type", image.getType());
				}
				
				int updateCount = query.executeUpdate();
				return updateCount > 0;
			}		
		}catch(Exception e){
			System.out.println("ERROR UDPATING IMAGE: "+ e);			
		}			
		return false;
	}
	@Override
	public List<Image> listImagesByProduct(Product product) {
		try{
	    	TypedQuery<Image> query = entityManager.createQuery("SELECT i FROM Image i WHERE i.product.id = :id", Image.class);  
			query.setParameter("id", product.getId());
			return query.getResultList();
		}catch(Exception e){
			System.out.println("ERROR LISTING PRODUCT IMAGES: "+ e);	
		}
		return new ArrayList<Image>();
	}
}