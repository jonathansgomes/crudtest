package br.com.jonathangomes.image.dao;

import java.util.List;

import br.com.jonathangomes.image.model.Image;
import br.com.jonathangomes.product.model.Product;

public interface ImageDao {
	
	List<Image> listImages();
	boolean insertImage(Image image);
	boolean deleteImage(Image image);
	boolean updateImage(Image image);
	List<Image> listImagesByProduct(Product product);

}
