package br.com.jonathangomes.image.vo;

import java.io.Serializable;

import br.com.jonathangomes.product.vo.ProductVO;

public class ImageVO implements Serializable{
	private static final long serialVersionUID = 1L;

	private int id;
	private String type;
    private ProductVO product;
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public ProductVO getProduct() {
		return product;
	}
	public void setProduct(ProductVO product) {
		this.product = product;
	}

}
