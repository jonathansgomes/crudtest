package br.com.jonathangomes.image.converter;

import java.util.ArrayList;
import java.util.List;

import br.com.jonathangomes.image.model.Image;
import br.com.jonathangomes.image.vo.ImageVO;
import br.com.jonathangomes.product.converter.ProductConverter;

public class ImageConverter {
	
	public static List<ImageVO> getVOList(List<Image> imageModels){
		List<ImageVO> imageVOs = new  ArrayList<ImageVO>();
		if(imageModels != null && !imageModels.isEmpty()){
			for(Image i : imageModels){
				ImageVO vo = getVO(i);
				if(vo != null){				
					imageVOs.add(vo);
				}
			}
		}
		return imageVOs;
	}
	public static ImageVO getVO(Image imageModel){
		ImageVO vo = null;
		if(imageModel != null){
			vo = new ImageVO();
			vo.setId(imageModel.getId());
			vo.setType(imageModel.getType());
			vo.setProduct(ProductConverter.getVO(imageModel.getProduct()));
		}
		return vo;
	}

	public static Image getModel(ImageVO vo){
		Image imageModel = null;
		if(vo != null){
			imageModel = new Image();
			imageModel.setId(vo.getId());
			imageModel.setType(vo.getType());
			imageModel.setProduct(ProductConverter.getModel(vo.getProduct()));
		}
		return imageModel;
	}
}
